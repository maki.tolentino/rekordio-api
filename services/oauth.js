const jwt = require('jsonwebtoken');

const KEY = 'SECRET_KEY';

const options = {
  issuer: 'rekordio-app',
  subject: 'sample-auth',
  audience: 'rekordio-app',
  expiresIn: '1h'
};

const authService = {

  isCorrectUser: function (user, password) {
    return (user === 'testUser' && password === 'testPassword');
  },

  generateToken: function (user) {
    return jwt.sign({
      user: user,
    }, KEY, options);
  },

  validateToken: function (token, currentDateTime) {
    try {
      const decoded = jwt.verify(token, KEY, options);
      return (decoded.exp < currentDateTime)
    } catch (e) {
      return false;
    }
  }
};

module.exports = authService;