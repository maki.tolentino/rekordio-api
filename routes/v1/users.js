const express = require('express');
const router = express.Router();
const authService = require('../../services/oauth');

const DEPRECATION_INFO = 'This endpoint is deprecated. Please use: <a> http://localhost:3000/v2/users </a> instead.';
const DEPRECATION_DATE = '2021-01-01T00:00:00.000Z';

router.get('/', function (req, res, next) {
  const token = req.headers['authorization'];
  const currentDateTime = new Date();
  if (token && authService.validateToken(token, currentDateTime)) {
    res.append('X-Deprecation-Info', DEPRECATION_INFO);
    res.append('X-Deprecation-Date', DEPRECATION_DATE);
    return res.send('Success!');
  }
  return res.sendStatus(401);
});

module.exports = router;
