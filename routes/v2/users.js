const express = require('express');
const router = express.Router();
const authService = require('../../services/oauth');

router.get('/', function (req, res, next) {
  const token = req.headers['authorization'];
  const currentDateTime = new Date();
  if (token && authService.validateToken(token, currentDateTime)) {
    return res.send('Success!');
  }
  return res.sendStatus(401);
});

module.exports = router;
