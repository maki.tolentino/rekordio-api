const express = require('express');
const router = express.Router();
const authService = require('../services/oauth');

router.post('/auth', function (req, res, next) {
  if (authService.isCorrectUser(req.body.user, req.body.password)) {
    const token = authService.generateToken(req.body.user);
    return res.send(token);
  }
  return res.sendStatus(401)
});

module.exports = router;
